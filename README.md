# VoterPeer Android App

Censorship resistant on-chain blockchain voting.

**[See Documentation and project details](https://docs.voter.cash)**

This repository is for the android app for casting votes, creating a blockchain identity, viewing election results and authenticating to other apps using your blockchain identity.

<div style="margin-left: auto; margin-right: auto; width: 80%;">
<a href="https://gitlab.com/bitcoinunlimited/votepeer-android-app/-/jobs/artifacts/master/raw/app-release.apk?job=assemble_release"><img src="apk-download-badge.png" width="33%">
</a>
<a href="https://play.google.com/store/apps/details?id=info.bitcoinunlimited.voting"><img src="https://cdn.rawgit.com/steverichey/google-play-badge-svg/master/img/en_get.svg" width="33%"></a>
</div>

## Building from source

For a release build, run `./gradlew assembleRelease`.

For development build, see section *for developers* below.

## For developers
#### [How to build the project](https://docs.voter.cash/android-build/)
#### [Running tests](https://docs.voter.cash/android-test/)