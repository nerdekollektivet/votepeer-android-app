package info.bitcoinunlimited.voting.wallet.mnemonic

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.savedstate.SavedStateRegistryOwner
import info.bitcoinunlimited.voting.utils.onEachEvent
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewIntent.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import info.bitcoinunlimited.voting.wallet.room.WalletDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
class MnemonicViewModel(
    internal val privateKeyHex: String,
    mnemonic: Mnemonic,
) : ViewModel() {
    internal val state = MutableStateFlow<MnemonicViewState?>(MnemonicAvailable(mnemonic))

    internal fun bindIntents(view: MnemonicView) {
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope)

        view.submitMnemonic().onEachEvent { submitMnemonic ->
            submitMnemonic(submitMnemonic)
        }.launchIn(viewModelScope)
    }

    internal fun submitMnemonic(submitMnemonic: SubmitMnemonic) {
        val mnemonic = Mnemonic(submitMnemonic.phrase)

        if (mnemonic.phrase.isEmpty())
            state.value = MnemonicErrorMessage("Mnemonic is empty")
        else if (!WalletDatabase.isWIF(mnemonic.phrase) && !WalletDatabase.isMnemonic(mnemonic.phrase)) {
            state.value = MnemonicErrorMessage("Phrase is not a mnemonic or a WIF")
        } else {
            state.value = MnemonicAvailable(mnemonic)
        }
    }

    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    class MMnemonicViewModelFactory(
        private val privateKeyHex: String,
        private val mnemonic: Mnemonic,
        owner: SavedStateRegistryOwner
    ) : AbstractSavedStateViewModelFactory(owner, null) {
        override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
            MnemonicViewModel(privateKeyHex, mnemonic) as T
    }
}
